#!/bin/bash

# Проверяем наличие параметра n
if [ $# -ne 2 ]; then
  echo "Использование: $0 <количество_запросов> <URL-адрес_сайта>"
  exit 1
fi

n=$1  # Количество запросов
url=$2  # URL-адрес для тестирования

total_time=0  # Общее время выполнения
avg_time=0  # Среднее время выполнения

# Цикл для выполнения n запросов
for ((i=1; i<=n; i++)); do
  echo "Запрос #$i"
  result=$(curl -o /dev/null -s -w "Время: %{time_total} сек" "$url")
  echo "$result"
  echo "----------------------"

  # Извлекаем время выполнения из результата curl
  time=$(echo "$result" | grep -oP "Время: \K\d+\.\d+")

  # Добавляем время выполнения к общему времени
  total_time=$(echo "$total_time + $time" | bc)
done

# Подсчитываем среднее время выполнения
if [ $n -gt 0 ]; then
  avg_time=$(echo "$total_time / $n" | bc -l)
fi

echo "Количество запросов: $n"
echo "Среднее время выполнения: $avg_time сек"
